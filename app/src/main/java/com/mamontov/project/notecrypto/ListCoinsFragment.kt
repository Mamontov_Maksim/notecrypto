package com.mamontov.project.notecrypto

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import com.mamontov.project.notecrypto.controler.Coin
import com.mamontov.project.notecrypto.controler.CoinLab
import java.util.*
import kotlin.collections.ArrayList as ArrayList1

/**
 * TODO (Добавить документацию)
 * Фрагмент наложенный на ListCoinsActivity
 *
 * В нем реализованна логика выведения всех крипт для дальнейщего его добавления в
 * список интересуещих вас крипт
 */
class ListCoinsFragment: Fragment() {

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: ListCoinsAdapter

    //TODO (список уже не нужен)
    private lateinit var mCoinsAll: ArrayList<Coin>
    private var mCoinLab = CoinLab.getInstance(activity)

    fun initFragment(): ListCoinsFragment{
        return ListCoinsFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ItemMarket().execute()
        updateUI()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view: View = inflater.inflate(R.layout.fragment_list_coins, container, false)

        mRecyclerView = view.findViewById(R.id.recycler_coins_list)
        mRecyclerView.layoutManager = GridLayoutManager(activity, 1)

        return view
    }

    fun updateUI(){
        if (isAdded){
            mAdapter = ListCoinsAdapter(mCoinsAll)
            mRecyclerView.adapter = mAdapter
        }
    }

    //TODO (Изменить стиль данного списка, сделать его более красивым)
    private inner class ListCoinsHolder(layoutInflater: LayoutInflater?, viewGroup: ViewGroup?):
            RecyclerView.ViewHolder(layoutInflater?.inflate(R.layout.item_list_coins, viewGroup, false)!!){

        var mCoinNameText: TextView
        var mIsFavoriteCheckBox: CheckBox

        lateinit var mCoin: Coin

        init {
            mCoinNameText = itemView.findViewById(R.id.item_coin_text)
            mIsFavoriteCheckBox = itemView.findViewById(R.id.is_favorite_check_box)
            mIsFavoriteCheckBox.setOnCheckedChangeListener(object: CompoundButton.OnCheckedChangeListener{
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    mCoin.mFavorite = isChecked
                    //TODO (обновялть таблицу по мере изменения желания пользвоателя)
                    if (isChecked) {
                        if (mCoinLab.getCoin(mCoin.mIdCoinMarket) == null ){
                            CoinLab.getInstance(activity).addCoin(mCoin)
                        }
                        mIsFavoriteCheckBox.setButtonDrawable(R.mipmap.ic_favorite_check_box_true)
                    } else {
                        mCoinLab.deletedCoin(mCoin)
                        mIsFavoriteCheckBox.setButtonDrawable(R.mipmap.ic_favorite_check_box_false)
                    }
                }
            })
        }

        fun bind(coin: Coin){
            mCoin = coin
            mCoinNameText.text = "${mCoin.mName} (${mCoin.mSymbol})"
            mIsFavoriteCheckBox.isChecked = mCoinLab.getCoin(mCoin.mIdCoinMarket) != null
        }
    }

    private inner class ListCoinsAdapter(coins: ArrayList<Coin>): RecyclerView.Adapter<ListCoinsHolder>(){

        var mCoins: ArrayList<Coin>

        init {
            mCoins = coins
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListCoinsHolder {
            val layoutInflater = LayoutInflater.from(activity)
            return ListCoinsHolder(layoutInflater, viewGroup)
        }

        override fun onBindViewHolder(listHolder: ListCoinsHolder, index: Int) {
            var coin = mCoins.get(index)
            listHolder.bind(coin)
        }

        override fun getItemCount(): Int {
            return mCoins.size
        }
    }

    //TODO (Подумать над тем нужен ли он.) СКОРЕЕ ВСЕГО AsyncTask стоит удалить
    private inner class ItemMarket: AsyncTask<Void, Void, ArrayList<Coin>>() {
        override fun doInBackground(vararg params: Void?): ArrayList<Coin> {
            return CoinMarket().marketItems()
        }

        override fun onPostExecute(result: ArrayList<Coin>) {
            mCoinsAll = result
            updateUI()
        }
    }
}