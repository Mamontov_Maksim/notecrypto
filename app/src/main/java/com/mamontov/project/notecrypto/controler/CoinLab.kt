package com.mamontov.project.notecrypto.controler

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.mamontov.project.notecrypto.dateBase.CoinCursorWrapper
import com.mamontov.project.notecrypto.dateBase.SQLiteHelper
import com.mamontov.project.notecrypto.dateBase.Schema
import java.util.ArrayList

/**
 * Класс синглет через которого идет обращение в БД.
 * Служит для основной таблицы в которой хранятся основные и самые НОВАЯ стоимость
 * той или иной криптоВалюты
 *
 * Используется класс в виде хранения данных "Coin"
 *
 * @author Maksim
 */
class CoinLab private constructor(context: Context?){

    lateinit var mContext: Context
    lateinit var mDataBase: SQLiteDatabase

    init {
        if (context != null) {
            mContext = context.applicationContext
            mDataBase = SQLiteHelper(mContext).writableDatabase
        }
    }

    companion object {
        var mCoinLab: CoinLab? = null

        fun getInstance(context: Context?): CoinLab{
            if (mCoinLab == null){
                mCoinLab = CoinLab(context)
            }
            return mCoinLab as CoinLab
        }
    }

    /**
     * Используется для получения отдельного обьекта из основной таблицы
     *
     * @param idCoinMarket = Индификатор с биржи CoinMarket. Этот индификатор уникален для каждой
     * крипто валюты.
     *
     * @return Coin = Возвращает обьект в котором хранятся все данный той или иной криптоВалюты
     */
    fun getCoin(idCoinMarket: String?): Coin? {
        val cursor = queryCoin(Schema.TABLE.COLS().COIN_ID_MARKET + " =?",
                arrayOf(idCoinMarket!!))
        try {
            if (cursor.getCount() == 0) {
                return null
            }
            cursor.moveToFirst()
            return cursor.getCoin()
        } finally {
            cursor.close()
        }
    }

    /**
     * Используется для получения всех без исключений обьектов из таблицы
     *
     * @return ArrayList<Coin>
     */
    fun getCoins(): ArrayList<Coin> {
        val cursor = queryCoin(null, null)
        val listCoins = ArrayList<Coin>()

        try {
            cursor.moveToFirst()
            while (!cursor.isAfterLast) {
                listCoins.add(cursor.getCoin())
                cursor.moveToNext()
            }
            return listCoins
        } finally {
            cursor.close()
        }

    }

    //TODO (Метод getFavoriteCoins. Для выведения всех обьектов у которых Coin.mFavorite = true)

    /**
     * Добавляется в таблицу крипту с уникальным индификатором
     *
     * @param coin = Заполенный обьект
     */
    fun addCoin(coin: Coin) {
        val values = getContentValues(coin)
        mDataBase.insert(Schema.TABLE().TABLE_NAME, null, values)
    }

    /**
     * Обновляет данные таблицы
     */
    fun updateCoin(coin: Coin) {
        val values = getContentValues(coin)
        mDataBase.update(Schema.TABLE().TABLE_NAME, values,
                Schema.TABLE.COLS().COIN_ID_MARKET + " =?",
                arrayOf(coin.mIdCoinMarket))
    }

    /**
     * Удаляет данный с таблицы
     */
    fun deletedCoin(coin: Coin) {
        mDataBase.delete(Schema.TABLE().TABLE_NAME,
                Schema.TABLE.COLS().COIN_ID_MARKET + " =?",
                arrayOf(coin.mIdCoinMarket))
    }

    /**
     * Для получения общей стоимости вашего кошелька используется этот метод.
     * Он листает по всему списку и умножая количество криптоВалюты на ее стоимость
     *
     * @return Double - общую цену ващего кошелька
     */
    fun totalCount(): Double {
        var totalCount: Double = 0.0

        for (coins in getCoins()) {
            totalCount += coins.mTotalCount
        }

        return totalCount
    }

    /**
     * Метод в котором заполняется пулл данных для внесения в таблицу
     * используется методами addCoin и updateCoin
     */
    private fun getContentValues(coin: Coin): ContentValues {
        val values = ContentValues()
        values.put(Schema.TABLE.COLS().COIN_ID_MARKET, coin.mIdCoinMarket)
        values.put(Schema.TABLE.COLS().COIN_NAME, coin.mName)
        values.put(Schema.TABLE.COLS().COIN_SYMBOL, coin.mSymbol)
        values.put(Schema.TABLE.COLS().COIN_COUNT, coin.mTotalCount)
        if (coin.mFavorite) {
            values.put(Schema.TABLE.COLS().COIN_FAVORITE, 1)
        } else {
            values.put(Schema.TABLE.COLS().COIN_FAVORITE, 0)
        }
        return values
    }

    /**
     * Метод для получения курсора. Который помогает проходить как по каждому элементу
     * в таблице так и по отдельному. В основном ипользуется индефекатор криптоВалюты.
     *
     * @param whereClause - параметр запроса по определенному столбцу
     * @param whereArgs - параметр запроса для определенного значения обьекта,
     * уберая не нужные нам элементы и оставляя только один
     */
    private fun queryCoin(whereClause: String?, whereArgs: Array<String>?): CoinCursorWrapper {
        val cursor = mDataBase.query(Schema.TABLE().TABLE_NAME, null, whereClause,
                whereArgs, null, null, null)
        return CoinCursorWrapper(cursor)
    }
}