package com.mamontov.project.notecrypto

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.util.Log

class CoinActivity : SingleActivityFragment() {

    val EXTRA_ID = "_id"

    fun newIntent(context: Context?, id: String): Intent{
        var intent = Intent(context, this.javaClass)
        intent.putExtra(EXTRA_ID, id)
        return intent
    }

    override fun createFragment(): Fragment {
        var id = this.intent.getSerializableExtra(EXTRA_ID) as String
        return CoinFragment().initFragment(id)
    }
}