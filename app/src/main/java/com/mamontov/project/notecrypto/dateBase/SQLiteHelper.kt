package com.mamontov.project.notecrypto.dateBase

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
//TODO (Добавления документации)
class SQLiteHelper(context: Context): SQLiteOpenHelper(context, "MySqlIt.db",null, 1) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("CREATE TABLE ${Schema.TABLE().TABLE_NAME} (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " ${Schema.TABLE.COLS().COIN_ID_MARKET}," +
                " ${Schema.TABLE.COLS().COIN_NAME}," +
                " ${Schema.TABLE.COLS().COIN_SYMBOL}," +
                " ${Schema.TABLE.COLS().COIN_COUNT}," +
                " ${Schema.TABLE.COLS().COIN_AMOUNT}," +
                " ${Schema.TABLE.COLS().COIN_FAVORITE})")

        db.execSQL("CREATE TABLE ${Schema.TABLE_HISTORY_TRANSACTION().TABLE_NAME} (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " ${Schema.TABLE_HISTORY_TRANSACTION.COLS().ID_MARKET}," +
                " ${Schema.TABLE_HISTORY_TRANSACTION.COLS().UUID}," +
                " ${Schema.TABLE_HISTORY_TRANSACTION.COLS().DATE}," +
                " ${Schema.TABLE_HISTORY_TRANSACTION.COLS().BUY_COUNT}," +
                " ${Schema.TABLE_HISTORY_TRANSACTION.COLS().AMOUNT})")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}