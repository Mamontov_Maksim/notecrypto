package com.mamontov.project.notecrypto

import android.support.v4.app.Fragment

class ListCoinsActivity: SingleActivityFragment() {

    override fun createFragment(): Fragment {
        return ListCoinsFragment().initFragment()
    }
}