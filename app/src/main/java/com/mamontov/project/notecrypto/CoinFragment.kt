package com.mamontov.project.notecrypto

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

import com.mamontov.project.notecrypto.controler.*
import kotlin.Exception

//TODO (Добавления документации)
class CoinFragment : Fragment() {

    private val EXTRA_ID = "fragment_id"

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mTextTotalCount: TextView
    private lateinit var mNameCryptoTextView: TextView
    private lateinit var mPercentDifferenceCountTextView: TextView
    private lateinit var mButtonAddBy: Button
    private lateinit var mButtonRefresh: Button

    private lateinit var mCoin: Coin
    private lateinit var mHistoryCoin: HistoryCoin
    private lateinit var mCoinLab: CoinLab

    private var mCount: Double = 0.0

    fun initFragment(id: String): CoinFragment {
        val bundle = Bundle()
        bundle.putString(EXTRA_ID, id)
        val coin = CoinFragment()
        coin.arguments = bundle
        return coin
    }

    /**
     *
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id: String? = arguments?.getString(EXTRA_ID)
        mCoinLab = CoinLab.getInstance(activity)
        mCoin = mCoinLab.getCoin(id)!!
        mHistoryCoin = HistoryCoin()
        mHistoryCoin.mIdCoinMarket = mCoin.mIdCoinMarket
    }

    /**
     *
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_coin, container, false)

        mRecyclerView = view.findViewById(R.id.recycler_history_by)
        mRecyclerView.layoutManager = GridLayoutManager(activity, 1)

        mTextTotalCount = view.findViewById(R.id.total_money_text)


        mNameCryptoTextView = view.findViewById(R.id.name_crypto_text)
        mNameCryptoTextView.text = " ${mCoin.mName}:"

        mPercentDifferenceCountTextView = view.findViewById(R.id.persent_and_difference_count_text)

        mButtonAddBy = view.findViewById(R.id.button_add_by)
        mButtonAddBy.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val dialogBuilder = AlertDialog.Builder(activity)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialogBuilder.setMessage("Создание транзакции:")
                            .setCancelable(false)
                            .setNegativeButton("Нет", object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, which: Int) {
                                    dialog?.cancel()
                                }

                            })
                            .setPositiveButton("Да", object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, which: Int) {
                                    //перезапускаю фрагмент
                                    mHistoryCoin.mIdCoinMarket = mCoin.mIdCoinMarket
                                    HistoryCoinLab.getInstate(activity).add(mHistoryCoin)
                                    mCoin.mTotalCount = HistoryCoinLab.getInstate(activity).totalCount(mCoin.mIdCoinMarket)
                                    CoinLab.getInstance(activity).updateCoin(mCoin)
                                    val tr = fragmentManager?.beginTransaction()
                                    tr?.replace(R.id.fragment_container,
                                            initFragment(mCoin.mIdCoinMarket))
                                    tr?.commit()
                                    dialog?.cancel()
                                }

                            })
                            .setView(onCreateViewDialog(R.layout.dialog_icon_coin))
                    val dialog = dialogBuilder.create()
                    dialog.setTitle("Желаете сохранить транзакцию?")
                    dialog.show()
                }

            }

        })

        mButtonRefresh = view.findViewById(R.id.refresh_button)
        mButtonRefresh.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                CoinAsync().execute()
            }

        })
        updateUI()
        return view
    }

    /**
     * TODO исправить визуальность!
     * Метод который создает layout для Dialog
     */
    fun onCreateViewDialog(layoutResources: Int): View {
        val view = activity?.layoutInflater?.inflate(layoutResources, null)

        val mTotalMoneyEditText = view?.findViewById(R.id.count_edit_text) as EditText
        mTotalMoneyEditText.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    if (!s?.equals("")!!) {
                        mHistoryCoin.mBuyCount = s.toString().toDouble()
                    } else {
                        mHistoryCoin.mBuyCount = 0.0
                    }
                } catch (e: Exception) {
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

        val mTotalAmountEditText = view.findViewById(R.id.amount_edit_text) as EditText
        mTotalAmountEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    if (!s?.equals("")!!) {
                        mHistoryCoin.mAmount = s.toString().toDouble()
                    } else {
                        mHistoryCoin.mAmount = 0.0
                    }
                } catch (e: Exception) {

                }
            }
        })
        return view
    }

    fun updateUI(){
        if(isAdded){
                var array = HistoryCoinLab.getInstate(activity).getListHistory(mCoin.mIdCoinMarket)
                mRecyclerView.adapter = CoinAdapter(array)
        }
        var totalCount: Double? = mCoinLab.getCoin(mCoin.mIdCoinMarket)?.mTotalCount
        mTextTotalCount.text = " ${String.format("%.2f", totalCount)} USD"
    }

    inner class CoinHolder(inflater: LayoutInflater,container: ViewGroup):
            RecyclerView.ViewHolder(inflater.inflate(R.layout.item_coin_list_history, container, false)){

        private var mAmountHistoryTextView: TextView
        private var mCountBuyHistoryTextView: TextView
        private var mCoinHistoryDeletedButton: Button

        init {
            mAmountHistoryTextView = itemView.findViewById(R.id.amount_history_text)
            mCoinHistoryDeletedButton = itemView.findViewById(R.id.coin_history_deleted_button)

            mCountBuyHistoryTextView = itemView.findViewById(R.id.count_buy_history_text)


        }

        fun bind(coin: HistoryCoin){
            mAmountHistoryTextView.text = "${coin.mAmount}"
            mCountBuyHistoryTextView.text = "${String.format("%.2f", coin.mBuyCount)}USD"
            mCoinHistoryDeletedButton.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    HistoryCoinLab.getInstate(activity).deleted(coin.mID)
                    mCoin.mTotalCount = HistoryCoinLab.getInstate(activity).totalCount(mCoin.mIdCoinMarket)
                    CoinLab.getInstance(activity).updateCoin(mCoin)
                    updateUI()
                }

            })
        }
    }

    inner class CoinAdapter(coins: ArrayList<HistoryCoin>): RecyclerView.Adapter<CoinHolder>(){
        private var mCoinsHistory: ArrayList<HistoryCoin>
        init {
            mCoinsHistory = coins
        }
        override fun onCreateViewHolder(container: ViewGroup, p1: Int): CoinHolder {
            var inflater = LayoutInflater.from(activity)
            return CoinHolder(inflater, container)
        }

        override fun onBindViewHolder(coinHolder: CoinHolder, position: Int) {
            coinHolder.bind(mCoinsHistory.get(position))
        }

        override fun getItemCount(): Int {
            return mCoinsHistory.size
        }
    }

    /**
     *
     */
    inner class CoinAsync : AsyncTask<Void, Void, ArrayList<Coin>>() {
        override fun doInBackground(vararg params: Void?): ArrayList<Coin> {
            return CoinMarket().marketItems()
        }

        @SuppressLint("ResourceAsColor", "SetTextI18n")
        override fun onPostExecute(coins: java.util.ArrayList<Coin>) {
            for (coin in coins) {
                if (coin.mIdCoinMarket.equals(mCoin.mIdCoinMarket)) {
                    mCount = coin.mTotalCount
                    break
                }
            }
//            var rezult = mCoin.mAmount * mCount
//            mTextTotalCount.text = "${String.format("%.2f", rezult)} USD"
//            /**
//             * В этом блоке кода выводится иформация по поводу того сколько заработал\потерял и
//             * общая сумма на данный момент
//             */
//            if (mCoin.mCount <= mCount){
//                mPercentDifferenceCountTextView.text = "+${String.format("%.2f",rezult - mCoin.totalCount())}USD"+
//                        "(${String.format("%.2f" , ((rezult - mCoin.totalCount())/rezult) * 100)}%)"
//                mTextTotalCount.setTextColor(Color.parseColor("#09ae25"))
//                mPercentDifferenceCountTextView.setTextColor(Color.parseColor("#0ac92b"))
//            } else {
//                mPercentDifferenceCountTextView.text = "-${String.format("%.2f",mCoin.totalCount() - rezult)}USD"+
//                        "(${String.format("%.2f",(mCoin.totalCount() - rezult)/(mCoin.totalCount()) * 100)}%)"
//                mTextTotalCount.setTextColor(Color.parseColor("#ae0911"))
//                mPercentDifferenceCountTextView.setTextColor(Color.parseColor("#f70914"))
//            }
        }
    }
}
