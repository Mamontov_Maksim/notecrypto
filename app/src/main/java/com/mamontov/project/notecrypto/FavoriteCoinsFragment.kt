package com.mamontov.project.notecrypto

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.TextView
import android.content.Intent
import com.mamontov.project.notecrypto.controler.Coin
import com.mamontov.project.notecrypto.controler.CoinLab

//TODO (Добавления документации)
class FavoriteCoinsFragment: Fragment() {

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mTotalMoneyTextView: TextView


    private lateinit var mCoinLab: CoinLab

    fun initFragment(): FavoriteCoinsFragment{
        return FavoriteCoinsFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        mCoinLab = CoinLab.getInstance(context)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_favorite_coin, container, false)


        mRecyclerView =  view.findViewById(R.id.recycler_favorite_coin)
        mRecyclerView.layoutManager = GridLayoutManager(activity, 1)

        mTotalMoneyTextView = view.findViewById(R.id.total_money_text)
        mTotalMoneyTextView.text = "${mCoinLab.totalCount()} USD"
        updateUI()
        return view
    }

    override fun onResume() {
        super.onResume()
        updateUI()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    fun updateUI(){
        if(isAdded) {
            mRecyclerView.adapter = FavoriteCoinsAdapter(mCoinLab.getCoins())
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add_favorite_coin -> {
                var intent = Intent(activity, ListCoinsActivity().javaClass)
                startActivity(intent)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }


    private inner class FavoriteCoinsHolder(layoutInflater: LayoutInflater?, viewGroup: ViewGroup?)
        : RecyclerView.ViewHolder(layoutInflater?.inflate(R.layout.item_favorite_list, viewGroup, false)!!),
        View.OnClickListener{

        var mTextView : TextView
        lateinit var mCoin: Coin

        init {
            mTextView = itemView.findViewById(R.id.item_favorite_coin_text)
            mTextView.setOnClickListener(this)
        }

        fun bind(coin: Coin){
            mCoin = mCoinLab.getCoin(coin.mIdCoinMarket)!!
            mTextView.text = "${mCoin.mName} (${mCoin.mSymbol}) : ${mCoin.mTotalCount} USD"

        }

        override fun onClick(v: View?) {
            when(v?.id){
                R.id.item_favorite_coin_text -> {
                    var intent = CoinActivity().newIntent(activity, mCoin.mIdCoinMarket)
                    startActivity(intent)
                }
            }
        }

    }

    private inner class FavoriteCoinsAdapter(coins: ArrayList<Coin>): RecyclerView.Adapter<FavoriteCoinsHolder>(){

        var mCoins = ArrayList<Coin>()

        init {
            mCoins = coins
        }

        override fun onCreateViewHolder(container: ViewGroup, p1: Int): FavoriteCoinsHolder {
            var layoutInflater = (LayoutInflater.from(activity))
            return FavoriteCoinsHolder(layoutInflater, container)
        }

        override fun onBindViewHolder(mainHolder: FavoriteCoinsHolder, id: Int) {
            mainHolder.bind(mCoins.get(id))
        }

        override fun getItemCount(): Int {
            return mCoins.size
        }

    }
}