package com.mamontov.project.notecrypto

import android.net.Uri
import android.util.Log
import com.mamontov.project.notecrypto.controler.Coin
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
//TODO (Добавления документации)
class CoinMarket {

    val API_KEY = "627a32ca-5c66-454c-9f05-cd057e4b05e4"

    var mCoins = ArrayList<Coin>()
    /**
     * Получения массива байтов для прочитания и выведения JSON
     */
    @Throws(IOException::class)
    fun getUrlBytes(urlSpec: String): ByteArray {
        val url = URL(urlSpec)
        val connection = url.openConnection() as HttpURLConnection
        try {
            val out = ByteArrayOutputStream()
            val int = connection.inputStream
            if (connection.responseCode != HttpURLConnection.HTTP_OK) {
                throw IOException(connection.responseMessage +
                        ": with " +
                        urlSpec)
            }
            var bytesRead = 0
            val buffer = ByteArray(1024)
            bytesRead = int.read(buffer)
            while (bytesRead > 0) {
                out.write(buffer, 0, bytesRead)
                bytesRead = int.read(buffer)
            }
            out.close()
            return out.toByteArray()
        } finally {
            connection.disconnect()
        }
    }

    fun getStringUrl(urlSuspect: String): String {
        return String(getUrlBytes(urlSuspect))
    }

    /**
     *
     */
    fun marketItems(): ArrayList<Coin>{
        var url = Uri.parse("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest")
                .buildUpon()
                .appendQueryParameter("CMC_PRO_API_KEY", API_KEY)
                .appendQueryParameter("start", "1")
                .appendQueryParameter("limit", "500")
                .appendQueryParameter("convert", "USD")
                .build().toString()
        var jsonString = getStringUrl(url)
        getCoins(mCoins, JSONObject(jsonString))
        return mCoins
    }

    /**
     *
     */
    private fun getCoins(items: ArrayList<Coin>, json: JSONObject){
        val jsonArray = json.optJSONArray("data")

        for (i in 0 until jsonArray.length()) {
            val firstJsonObject = jsonArray.getJSONObject(i)
            val coin = Coin()
            coin.mIdCoinMarket = firstJsonObject.getInt("id").toString()
            coin.mSymbol = firstJsonObject.getString("symbol")
            coin.mName = firstJsonObject.getString("name")
            val secondJsonObject = firstJsonObject.getJSONObject("quote")
            val threeJsonObject = secondJsonObject.getJSONObject("USD")
            var count = threeJsonObject.getDouble("price")
            //TODO (поменять формат добавления стоимости до 4 знаков после точки)
            //TODO (почему не работает String.format?:( Он делает число таким 123,32 вместо 123.32)
            coin.mTotalCount = count
            items.add(coin)
        }

    }
}