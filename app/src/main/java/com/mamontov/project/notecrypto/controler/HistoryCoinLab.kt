package com.mamontov.project.notecrypto.controler

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.mamontov.project.notecrypto.dateBase.CoinCursorWrapper
import com.mamontov.project.notecrypto.dateBase.SQLiteHelper
import com.mamontov.project.notecrypto.dateBase.Schema
import java.util.*
import kotlin.collections.ArrayList

//TODO (Добавления документации)
//TODO Реализация обращения с побочной бд
class HistoryCoinLab private constructor(context: Context?){

    lateinit var mContext: Context
    lateinit var mDataBase: SQLiteDatabase

    init {
        if (context != null) {
            mContext = context.applicationContext
            mDataBase = SQLiteHelper(mContext).writableDatabase
        }
    }

    companion object {
        var mCoinLab: HistoryCoinLab? = null

        fun getInstate(context: Context?): HistoryCoinLab{
            if (mCoinLab == null){
                mCoinLab = HistoryCoinLab(context)
            }
            return mCoinLab as HistoryCoinLab
        }
    }

    fun getListHistory(idCoinMarket: String): ArrayList<HistoryCoin> {
        var cursor = queryCoin(Schema.TABLE_HISTORY_TRANSACTION.COLS().ID_MARKET + " =?",
                                arrayOf(idCoinMarket))
        var coins = ArrayList<HistoryCoin>()
        try {
            cursor.moveToFirst()
            while (!cursor.isAfterLast){
                coins.add(cursor.getHistoryCoin())
                cursor.moveToNext()
            }
            return coins
        } finally {
            cursor.close()
        }
    }

    fun add(coin: HistoryCoin){
        mDataBase.insert(Schema.TABLE_HISTORY_TRANSACTION().TABLE_NAME,
                null, getContentValues(coin))
    }

    fun deleted(uuid: UUID){
        mDataBase.delete(Schema.TABLE_HISTORY_TRANSACTION().TABLE_NAME,
                Schema.TABLE_HISTORY_TRANSACTION.COLS().UUID + " =?",
                        arrayOf(uuid.toString()))
    }

    fun totalCount(idCoinMarket: String): Double{
        var rezult = 0.0
        for (historyCoin in getListHistory(idCoinMarket)){
            rezult += historyCoin.mBuyCount * historyCoin.mAmount
        }
        return rezult
    }

    /**
     * Метод в котором заполняется пулл данных для внесения в таблицу
     * используется методами addCoin
     */
    private fun getContentValues(coin: HistoryCoin): ContentValues {
        val values = ContentValues()
        values.put(Schema.TABLE_HISTORY_TRANSACTION.COLS().ID_MARKET, coin.mIdCoinMarket)
        values.put(Schema.TABLE_HISTORY_TRANSACTION.COLS().UUID, coin.mID.toString())
        values.put(Schema.TABLE_HISTORY_TRANSACTION.COLS().DATE, coin.mDate.toString())
        values.put(Schema.TABLE_HISTORY_TRANSACTION.COLS().BUY_COUNT, coin.mBuyCount)
        values.put(Schema.TABLE_HISTORY_TRANSACTION.COLS().AMOUNT, coin.mAmount)
        return values
    }


    /**
     * Метод для получения курсора. Который помогает проходить как по каждому элементу
     * в таблице так и по отдельному. В основном ипользуется индефекатор криптоВалюты.
     *
     * @param whereClause - параметр запроса по определенному столбцу
     * @param whereArgs - параметр запроса для определенного значения обьекта,
     * уберая не нужные нам элементы и оставляя только один
     */
    private fun queryCoin(whereClause: String?, whereArgs: Array<String>?): CoinCursorWrapper {
        val cursor = mDataBase.query(Schema.TABLE_HISTORY_TRANSACTION().TABLE_NAME, null, whereClause,
                whereArgs, null, null, null)
        return CoinCursorWrapper(cursor)
    }
}