package com.mamontov.project.notecrypto.dateBase

import android.database.Cursor
import android.database.CursorWrapper
import com.mamontov.project.notecrypto.controler.Coin
import com.mamontov.project.notecrypto.controler.HistoryCoin
import java.util.*

//TODO (Добавления документации)
class CoinCursorWrapper(cursor: Cursor): CursorWrapper(cursor) {

    fun getCoin(): Coin {
        var idString = getString(getColumnIndex(Schema.TABLE.COLS().COIN_ID_MARKET))
        var coinName = getString(getColumnIndex(Schema.TABLE.COLS().COIN_NAME))
        var coinSymbol = getString(getColumnIndex(Schema.TABLE.COLS().COIN_SYMBOL))
        var coinCount = getDouble(getColumnIndex(Schema.TABLE.COLS().COIN_COUNT))
        var coinAmount = getDouble(getColumnIndex(Schema.TABLE.COLS().COIN_AMOUNT))
        var isFavorite = getInt(getColumnIndex(Schema.TABLE.COLS().COIN_FAVORITE))

        var coin = Coin()
        coin.mIdCoinMarket = idString
        coin.mName = coinName
        coin.mSymbol = coinSymbol
        coin.mTotalCount = coinCount
        coin.mFavorite = isFavorite == 1
        return coin
    }

    fun getHistoryCoin(): HistoryCoin{
        var idString = getString(getColumnIndex(Schema.TABLE_HISTORY_TRANSACTION.COLS().ID_MARKET))
        var id = getString(getColumnIndex(Schema.TABLE_HISTORY_TRANSACTION.COLS().UUID))
        var coinAmount = getDouble(getColumnIndex(Schema.TABLE_HISTORY_TRANSACTION.COLS().AMOUNT))
        var coinCount = getDouble(getColumnIndex(Schema.TABLE_HISTORY_TRANSACTION.COLS().BUY_COUNT))

        var coin = HistoryCoin(UUID.fromString(id))
        coin.mIdCoinMarket = idString
        coin.mAmount = coinAmount
        coin.mBuyCount = coinCount
        return coin
    }

}