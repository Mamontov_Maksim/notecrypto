package com.mamontov.project.notecrypto

import android.support.v4.app.Fragment

class FavoriteCoinsActivity : SingleActivityFragment() {

    override fun createFragment(): Fragment {
        return FavoriteCoinsFragment().initFragment()
    }
}