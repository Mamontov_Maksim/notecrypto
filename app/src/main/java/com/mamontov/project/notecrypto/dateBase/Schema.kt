package com.mamontov.project.notecrypto.dateBase

class Schema {
    class TABLE {
        val TABLE_NAME = "FavoriteCryptoCoin"
        class COLS {
            val COIN_ID_MARKET = "idCoinMarket"
            val COIN_NAME = "coinName"
            val COIN_SYMBOL = "coinSymbol"
            val COIN_COUNT = "coinCount"
            val COIN_AMOUNT = "coinAmount"
            val COIN_FAVORITE = "favoriteCheck"
        }
    }

    class TABLE_HISTORY_TRANSACTION{
        val TABLE_NAME = "TransactionCoin"
        class COLS {
            val ID_MARKET = "id_coin_market"
            val UUID = "uuid"
            val DATE = "history_date"
            val BUY_COUNT = "buy_count"
            val AMOUNT = "amount"
        }
    }
}